import { useState } from "react";

import RenderLoader from "@/components/Render";
import GridCard from "@/components/GridCard";
import GridCardHeader from "@/components/GridCard/Header";

const maxLevel = 4;
const levels = new Array(maxLevel).fill(0).map((e, i) => {
    return {
        gap: 10,
        level: i,
        point: 0,
        pointMax: 0,
        speed: 1000 - i * 50,
    };
});

const Index = ({ local, levelSelect }) => {
    const [level, setLevel] = useState(levelSelect);
    const [config, setConfig] = useState({
        ...levels[level],
        ...(local[level ?? "a"] ?? {}),
        point: 0,
    });
    const upPoint = () => {
        const newP = Math.max(config.point + 1, config.pointMax, 0);
        const newConfig = {
            ...config,
            point: config.point + 1,
            pointMax: newP,
        };
        setConfig(newConfig);
        localStorage.setItem(
            "localData",
            JSON.stringify({ ...local, [level]: newConfig })
        );
    };
    const exitGame = () => {
        const repit = confirm("Perdiste, Volver a Jugar?");
        if (repit) {
            window.location.reload();
        }
    };
    return (
        <>
            <GridCardHeader {...config} />
            <GridCard {...config} upPoint={upPoint} exitGame={exitGame} />
        </>
    );
};

const Render = () => {
    const [props, setProps] = useState({});
    const loadPage = async (local) => {
        const levelSelect = local.levelSelect ?? 0;
        setProps({
            local,
            levelSelect,
        });
        return "ok";
    };
    return (
        <>
            <RenderLoader loadPage={loadPage} loader321={true}>
                <Index {...props} />
            </RenderLoader>
        </>
    );
};

export default Render;
