import { useState } from "react";
import { useRouter } from "next/router";

import RenderLoader from "@/components/Render";

const maxLevel = 4;
const Index = ({ local, levelSelect }) => {
    const router = useRouter();
    const configPage = {
        width: "min(100vh,100vw)",
        padding: `10px`,
    };
    const levels = new Array(maxLevel).fill(0).map((e, i) => {
        return {
            gap: 10,
            level: i,
            point: 0,
            pointMax: 0,
            speed: 1000 - i * 50,
            ...(local[i ?? "a"] ?? {}),
        };
    });
    const selectLevel = (i) => () => {
        localStorage.setItem(
            "localData",
            JSON.stringify({ ...local, levelSelect: i })
        );
        router.push("/game");
    };
    return (
        <div
            className="m-auto height-vh-min-100 flex flex-align-content-center flex-justify-center"
            style={configPage}
        >
            <h1 className="color-white font-w-900 font-montserrat font-49 text-center width-p-100 m-b-25">
                Memoria
            </h1>
            {levels.map((e, i) => {
                return (
                    <div
                        key={i}
                        onClick={selectLevel(i)}
                        className="width-p-100 flex flex-justify-between color-white font-w-900 font-montserrat font-20 m-b-25 p-v-35 p-h-45 border-style-solid border-5 border-radius-20 pointer bg-SHIBUYA bg-TIGER-hover"
                    >
                        <h4>Level {e.level + 1}</h4>
                        <h4>Puntuacion Maxima {e.pointMax}</h4>
                    </div>
                );
            })}
        </div>
    );
};

const Render = () => {
    const [props, setProps] = useState({});
    const loadPage = async (local) => {
        const levelSelect = local.levelSelect ?? 0;
        setProps({
            local,
            levelSelect,
        });
        return "ok";
    };
    return (
        <>
            <RenderLoader loadPage={loadPage}>
                <Index {...props} />
            </RenderLoader>
        </>
    );
};

export default Render;
