import GridCardBase from "@/components/GridCard/Base";

const GridCard = (props) => {
    return <GridCardBase {...props} level={props.level + 1} />;
};
export default GridCard;
