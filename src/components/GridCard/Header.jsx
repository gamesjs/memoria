import { useRouter } from "next/router";

import Button from "@/components/Buttons";

const GridCardHeader = ({ ...props }) => {
    const router = useRouter();
    const config = {
        gap: 10,
        level: 1,
        point: 0,
        pointMax: 0,
        ...props,
    };
    const configGrid = {
        padding: `${config.gap}px`,
        width: "calc(min(100vh,100vw) - 50px)",
        // height: "min(100vh,100vw)",
        // minHeight: `calc(max(calc(100vh - 100vw),50px)`,
        display: "grid",
        gridTemplateColumns: `repeat(3, 1fr)`,
        alignItems: "center",
    };
    return (
        <div
            style={configGrid}
            className="color-white font-w-700 font-montserrat m-auto "
        >
            <div style={{ gridColumn: "1/4" }}>
                <Button
                    onClick={() => {
                        router.back();
                    }}
                >
                    Inicio
                </Button>
            </div>
            <div>Dificultad {config.level + 1}</div>
            <div className="text-center">Puntuacion {config.point}</div>
            <div className="text-right">
                Puntuacion Maxima {config.pointMax}
            </div>
        </div>
    );
};
export default GridCardHeader;
