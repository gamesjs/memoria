import { useState, useEffect } from "react";

import Card from "@/components/Card";

const GridCardBase = ({
    exitGame = () => {},
    upPoint = () => {},
    ...props
}) => {
    const config = {
        gap: 10,
        level: 3,
        speed: 1000,
        ...props,
    };
    const configGrid = {
        display: "grid",
        gridTemplateColumns: `repeat(${config.level * 2}, 1fr)`,
        gridTemplateRows: `repeat(${config.level * 2}, 1fr)`,
        gridColumnGap: `${config.gap}px`,
        gridRowGap: `${config.gap}px`,
        padding: `${config.gap}px`,
        width: "calc(min(100vh,100vw) - 50px)",
        height: "calc(min(100vh,100vw) - 50px)",
    };
    const nCards = (config.level * 2) ** 2;
    const GridBase = new Array(nCards).fill(false);
    const [grid, setGrid] = useState(GridBase);
    const [pointsAction, setPointsAction] = useState([]);
    const [pointClickCard, setPointClickCard] = useState(0);
    const [isActiveGame, setIsActiveGame] = useState(false);
    const [cardActive, setCardActive] = useState(-1);

    const addNewPoint = () => {
        setPointsAction([...pointsAction, parseInt(Math.random() * nCards)]);
    };

    const getColorActive = (i) => {
        const x = parseInt(i % (config.level * 2)) + 1;
        const y = parseInt(i / (config.level * 2)) + 1;
        const z = parseInt(Math.sqrt(x + y)) + 1;
        const xColor = 255 / x;
        const yColor = 255 / y;
        const zColor = z * (200 / (config.level * 2)) + 55;

        return `rgb(${xColor},${yColor},${zColor})`;
    };
    const ciclePoint = (i = 0) => {
        const newGrid = [...GridBase];
        setGrid(newGrid);
        if (i >= pointsAction.length) {
            setIsActiveGame(true);
            return;
        }
        setTimeout(() => {
            const newGrid = [...GridBase];
            newGrid[pointsAction[i]] = true;
            setGrid(newGrid);
            setTimeout(() => {
                ciclePoint(i + 1);
            }, config.speed / 2);
        }, config.speed / 2);
    };
    useEffect(() => {
        if (pointsAction.length > 0) {
            ciclePoint();
        }
    }, [pointsAction]);

    const cicleGame = () => {
        setIsActiveGame(false);
        setPointClickCard(0);
        setTimeout(() => {
            addNewPoint();
        }, config.speed);
    };
    const clickCard = (i) => () => {
        if (!isActiveGame) return;
        setPointClickCard(pointClickCard + 1);
        if (pointsAction[pointClickCard] == i) {
            setCardActive(i);
            setTimeout(() => {
                setCardActive(-1);
            }, config.speed / 4);
            if (pointClickCard + 1 == pointsAction.length) {
                cicleGame();
                upPoint(pointClickCard + 1);
            }
        } else {
            exitGame(pointsAction.length - 1);
        }
    };

    useEffect(() => {
        cicleGame();
    }, []);

    return (
        <div
            className={`GridCard m-auto`}
            style={{
                ...configGrid,
                filter: `brightness(${isActiveGame ? "1" : ".8"})`,
            }}
        >
            {grid.map((e, i) => {
                return (
                    <Card
                        key={i}
                        active={e || cardActive == i}
                        colorActive={getColorActive(i)}
                        onClick={clickCard(i)}
                    />
                );
            })}
        </div>
    );
};
export default GridCardBase;
