import ContentLoader from "react-content-loader";

const LoaderPage = () => {
    return (
        <div className="flex flex-align-center flex-justify-center height-vh-min-100">
            <ContentLoader
                viewBox="0 0 400 160"
                height={160}
                width={400}
                backgroundColor="transparent"
            >
                <circle cx="150" cy="86" r="12" />
                <circle cx="194" cy="86" r="12" />
                <circle cx="238" cy="86" r="12" />
            </ContentLoader>
        </div>
    );
};

export default LoaderPage;
