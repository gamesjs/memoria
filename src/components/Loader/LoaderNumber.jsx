const LoaderNumber = ({ number }) => {
    return (
        <div
            className="flex flex-align-center flex-justify-center height-vh-min-100 color-white font-montserrat font-w-900"
            style={{
                fontSize: "min(50vh,50vw)",
                opacity: 0.5,
            }}
        >
            {number}
        </div>
    );
};

export default LoaderNumber;
