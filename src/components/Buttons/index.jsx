import ButtonBase from "@/components/Buttons/Base";

const Index = (props) => {
    const config = { ...props };
    config.className = `
        width-p-100 
        p-h-15 p-v-10
        font-25 font-w-900 line-h-0
        font-montserrat
        color-NATO color-white-hover
        border-radius-20
        border-0
        bg-TIGER bg-black-hover
        flex flex-justify-center flex-align-center
        text-center
    `;
    config.classNameLoader = `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
    `;
    return <ButtonBase {...config} />;
};
export default Index;
