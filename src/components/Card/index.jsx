import CardBase from "@/components/Card/Base";

const Card = ({ active = false, colorActive = "TIGER", ...props }) => {
    const config = { ...props };

    config.className = `
        pos-r
        border-radius-10
        overflow-hidden
        box-shadow
        bg-SHIBUYA
        pointer
        ${
            active
                ? `
                    box-shadow-c-white  
                    box-shadow-s-5 
                    box-shadow-inset
                `
                : `
                    box-shadow-b-5
                    box-shadow-c-bunker
                `
        }
    `;
    config.classNameImg = `
        pos-a
        top-0
        left-0
        right-0
        bottom-0
        width-p-100
        height-p-100
    `;
    config.style = {
        transform: `rotateY(${active ? 180 : 0}deg)`,
        "--color-active": colorActive,
        background: `${active ? `var(--color-active)` : "var(--SHIBUYA)"}`,
    };

    return <CardBase {...config} />;
};
export default Card;
