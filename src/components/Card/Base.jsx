const CardBase = ({
    className = "",
    classNameImg = "",
    img = "",
    mode = "/base",
    ...props
}) => {
    return (
        <>
            <div className={`card ${className}`} {...props}>
                {/* <img
                    src={`/img${mode}/${img}`}
                    alt={img}
                    className={classNameImg}
                /> */}
            </div>
        </>
    );
};
export default CardBase;
