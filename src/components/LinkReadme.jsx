const Index = ({ id, text = "code" }) => {
    return (
        <>
            <a
                href={`https://gitlab.com/franciscoblancojn/templatenextjs#${id}`}
                className="font-16"
                target="_blank"
                rel="noreferrer"
            >
                {text}
            </a>
        </>
    );
};
export default Index;
