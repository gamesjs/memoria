import { useState, useEffect } from "react";
import * as Yup from "yup";

import log from "@/functions/log";

const Index = ({
    yup = Yup.string(),
    label = "",
    placeholder = "",
    defaultValue = null,
    value = null,
    options = [],
    classNameLabel = "",
    classNameInput = "",
    classNameIcon = "",
    classNameError = "",
    classNameOptions = "",
    classNameOption = "",
    classNameOptionDisabled = "",
    onChange = () => {},
    onBlur = () => {},
    onChangeValidate = (e) => e,
    props = {},
    icon = <></>,
}) => {
    const [statusInput, setStateInput] = useState("");
    const [error, setError] = useState("");
    const [valueInput, setValueInput] = useState(defaultValue?.text ?? "");
    const [valueSelect, setValueSelect] = useState(defaultValue ?? {});
    const [optionSelect, setOptionSelect] = useState(options);
    const [showOptions, setShowOptions] = useState(false);

    const validateValue = async (v) => {
        v = await onChangeValidate(v);
        yup.validate(v)
            .then(function (valid) {
                if (valid) {
                    setStateInput("ok");
                    setError("");
                }
            })
            .catch(function (error) {
                log("error", error, "red");
                setStateInput("error");
                setError(error.message);
            });
        return v;
    };

    const changeInput = (e) => {
        const v = e.target.value;
        setValueInput(v);
    };
    const blurInput = () => {
        onBlur(valueSelect);
        setTimeout(() => {
            setShowOptions(false);
        }, 100);
    };
    const select = async (v) => {
        setValueSelect(v);
        await validateValue(v);
        setValueInput(v.text);
        onChange(v);
        setShowOptions(false);
    };
    useEffect(() => {
        setOptionSelect(
            options.filter((e) => {
                return (
                    e.text
                        .toLocaleLowerCase()
                        .indexOf(valueInput.toLocaleLowerCase()) != -1
                );
            })
        );
    }, [valueInput]);
    return (
        <>
            <label className={classNameLabel}>
                <div>{label}</div>
                {error != "" && <div className={classNameError}>{error}</div>}
                <div>
                    <input
                        type="text"
                        className={`input ${classNameInput} ${statusInput}`}
                        placeholder={placeholder}
                        value={value ?? valueInput}
                        onChange={changeInput}
                        onBlur={blurInput}
                        onMouseDown={() => {
                            setShowOptions(true);
                            setOptionSelect(options);
                        }}
                        onInput={() => {
                            setShowOptions(true);
                        }}
                        {...props}
                    />
                    <select
                        className="pos-a top-0 left-0 width-p-100 height-p-100 opacity-0 d-sm-none"
                        style={{ opacity: 0 }}
                        onChange={(e) => {
                            select(options[e.target.value]);
                        }}
                    >
                        {options.length == 0 ? (
                            <option disabled={true}>Not options</option>
                        ) : (
                            <option disabled={true}>Select</option>
                        )}
                        {options.map((e, i) => {
                            return (
                                <option
                                    key={i}
                                    value={i}
                                    onClick={() => {
                                        select(e);
                                    }}
                                >
                                    {e.text}
                                </option>
                            );
                        })}
                    </select>
                    <span className={classNameIcon}>{icon}</span>
                </div>
                {showOptions && (
                    <ul className={classNameOptions}>
                        {optionSelect.length == 0 ? (
                            <li
                                className={`${classNameOption} ${classNameOptionDisabled}`}
                                disabled={true}
                            >
                                Not options
                            </li>
                        ) : (
                            <li
                                className={`${classNameOption} ${classNameOptionDisabled}`}
                                disabled={true}
                            >
                                Select
                            </li>
                        )}
                        {optionSelect.map((e, i) => {
                            return (
                                <li
                                    key={i}
                                    className={classNameOption}
                                    onClick={() => {
                                        select(e);
                                    }}
                                >
                                    {e.html ?? e.text}
                                </li>
                            );
                        })}
                    </ul>
                )}
            </label>
        </>
    );
};
export default Index;
