import { useEffect, useState } from "react";
const Index = ({ children ,className}) => {
    const [top, setTop] = useState(0);
    const loadScroll = () => {
        window.addEventListener("scroll", () => {
            setTop(window.scrollY);
        });
    };
    useEffect(() => {
        loadScroll();
    }, []);
    return (
        <>
            <div className={`pos-sk ${className}`} style={{ top: `${top}px` }}>
                {children}
            </div>
        </>
    );
};
export default Index;
