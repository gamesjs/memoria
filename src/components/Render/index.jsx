import { useState, useEffect } from "react";
import { getCookie } from "cookies-next";

import LoaderPage from "@/components/Loader/LoaderPage";
import LoaderNumber from "@/components/Loader/LoaderNumber";

const RenderLoader = ({
    title = "Title Page",
    validateLogin = false,
    loader321 = false,
    loadPage = async () => "ok",
    children,
    ...props
}) => {
    const [loadeNumber, setLoadeNumbers] = useState(-1);
    const [load, setLoad] = useState(false);

    const countLoaderNumber = (i = 3) => {
        if (i <= 0) {
            setLoad(true);
            return;
        }
        setLoadeNumbers(i);
        setTimeout(() => {
            countLoaderNumber(i - 1);
        }, 500);
    };

    const onLoad = async () => {
        const music = new Audio("adf.wav");
        const local = localStorage.getItem("localData") ?? "{}";
        await loadPage({
            ...JSON.parse(local),
            play: () => music.play(),
        });

        if (loader321) {
            countLoaderNumber();
        } else {
            setLoad(true);
        }
    };
    useEffect(() => {
        onLoad();
    }, []);

    return (
        <>
            {load ? (
                <div className="height-vh-min-100 flex flex-align-content-center">
                    {children}
                </div>
            ) : (
                <>
                    {loadeNumber == -1 ? (
                        <LoaderPage />
                    ) : (
                        <LoaderNumber number={loadeNumber} />
                    )}
                </>
            )}
        </>
    );
};

export default RenderLoader;
