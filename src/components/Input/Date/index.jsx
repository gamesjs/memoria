import InputBase from "@/components/Input/Date/Base";
import Style from "@/components/Input/Date/Style";

const Index = (props) => {
    const types = {
        default: "date",
        date: "date",
        month: "month",
    };

    const config = {
        ...props,
        ...(Style[props.Style] ?? Style.default),
        type: types[props.type] ?? types.default,
    };
    return (
        <>
            <InputBase {...config} />
        </>
    );
};
export default Index;
