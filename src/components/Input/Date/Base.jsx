import { useState } from "react";
import * as Yup from "yup";

import log from "@/functions/log";

const Index = ({
    yup = Yup.string(),
    label = "",
    placeholder = "",
    defaultValue = null,
    value = null,
    classNameLabel = "",
    classNameContentInput = "",
    classNameInput = "",
    classNameIcon = "",
    classNameError = "",
    onChange = () => {},
    onBlur = () => {},
    onChangeValidate = async (e) => e,
    onChangeValidateBeforeYup = async (e) => e,
    onChangeValidateAfterYup = async (e) => e,
    props = {},
    icon = <></>,
    type = "date"
}) => {
    const [statusInput, setStateInput] = useState("");
    const [error, setError] = useState("");
    const [valueInput, setValueInput] = useState(defaultValue ?? "");

    const validateValue = async (v) => {
        try {
            v = await onChangeValidate(v);
            await onChangeValidateBeforeYup(v);
        } catch (error) {
            log("error", error, "red");
            setStateInput("error");
            setError(error.message);
            return v;
        }
        yup.validate(v)
            .then(async function (valid) {
                if (valid) {
                    setStateInput("ok");
                    setError("");
                    try {
                        await onChangeValidateAfterYup(v);
                    } catch (error) {
                        log("error", error, "red");
                        setStateInput("error");
                        setError(error.message);
                        return;
                    }
                }
            })
            .catch(function (error) {
                log("error", error, "red");
                setStateInput("error");
                setError(error.message);
                return;
            });
        return v;
    };

    const changeInput = async (e) => {
        const v = await validateValue(e.target.value);
        setValueInput(v);
        onChange(v);
    };
    const blurInput = () => {
        validateValue(valueInput);
        onBlur();
    };
    return (
        <>
            <label className={classNameLabel}>
                <div>{label}</div>
                <div className={"pos-r "+classNameContentInput}>
                    <input
                        type="text"
                        className={`input ${classNameInput} ${statusInput}`}
                        placeholder={placeholder}
                        onChange={changeInput} 
                        value={value ?? valueInput}
                        {...props}
                    />
                    <span className={classNameIcon}>{icon}</span>
                    <input 
                        type={type}
                        onChange={changeInput} 
                        value={value ?? valueInput}
                        onBlur={blurInput}
                        className={`
                            input
                            input-date
                            pos-a
                            top-0
                            left-0
                            width-p-100
                            height-p-100
                            opacity-0
                        `} 
                    />
                </div>
                {error != "" && <div className={classNameError}>{error}</div>}
            </label>
        </>
    );
};
export default Index;
