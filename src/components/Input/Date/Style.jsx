export default {
    default: {
        classNameInput: `
            p-h-10 p-v-5 
            font-20 font-montserrat
            width-p-100 
            border-0 
            border-radius-0
            box-shadow box-shadow-inset box-shadow-s-1 box-shadow-black 
            outline-none 
        `,
        classNameLabel: `
            font-20 font-montserrat
        `,
        classNameError: `
            font-10 font-montserrat
            text-right
            p-v-3
            color-error
        `,
    },
    style1: {
        classNameInput: `
            p-h-10 p-v-5 
            font-16 font-montserrat
            width-p-100 
            border-0 
            border-radius-0
            box-shadow box-shadow-inset box-shadow-s-2 box-shadow-black 
            outline-none 
        `,
        classNameLabel: `
            font-18 font-montserrat
        `,
        classNameError: `
            font-8 font-montserrat
            text-right
            p-v-3
            color-error
        `,
    },
};
