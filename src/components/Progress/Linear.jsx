const Index = ({ p = 50 }) => {
    p = Math.min(Math.max(p,0),100)
    return (
        <>
            <div className="width-p-100 border-2 border-style-solid border-gray bg-gray border-radius-15">
                <div
                    className={`width-p-${p} height-20 bg-white border-radius-10 text-center font-montserrat`}
                >
                    {p}%
                </div>
            </div>
        </>
    );
};
export default Index;
