import * as Yup from "yup";

import DATAERROR from "@/data/error/inputs";

export default {
    name: Yup.string().required(DATAERROR.name.required),
    email: Yup.string()
        .email(DATAERROR.email.invalid)
        .required(DATAERROR.email.required),
    url: Yup.string()
        .url(DATAERROR.url.invalid)
        .required(DATAERROR.url.required),
    number: Yup.string(DATAERROR.number.invalid)
        .required(DATAERROR.number.required)
        .min(0, DATAERROR.number.min)
        .max(10, DATAERROR.number.max),
    password: Yup.string()
        .required(DATAERROR.password.required)
        .min(6, DATAERROR.password.min)
        .max(20, DATAERROR.password.max),
    options: Yup.object().shape({
        value: Yup.string().required(DATAERROR.options.required),
    }),
    date: Yup.string().required(DATAERROR.date.required),
};
