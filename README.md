# Template for Nextjs

It is a template for Nextjs, for used in Startscoinc, used NextJs and Sass

# Install

Open terminal and clone repository [https://gitlab.com/franciscoblancojn/templatenextjs.git](https://gitlab.com/franciscoblancojn/templatenextjs.git)

```bash
git clone https://gitlab.com/franciscoblancojn/templatenextjs.git
```

Rename Forlder

```bash
mv templatenextjs nameNewProyect
```

Move in Forlder

```bash
cd nameNewProyect
```

Reset git

```bash
rm -fr .git
git init
```

Add your remote repositoy Git

```bash
git remote add origin <server>
```

Add your commit

```bash
git add .
git commit -m "Initial Commit"
```

Push in your Repository

```bash
git push
```

Install dependencies

```bash
npm i
```

Run dev

```bash
npm run dev
```

# Structure

## api

It is forder for connect nextjs with your api

## Components

It is forder for create your components for proyect

### Buttons

[doc](https://templatenextjs.vercel.app/doc/buttons)
Import Required

```jsx
import Buttons from "@/components/Buttons";
```

#### Button Base

```jsx
<Buttons>Button</Buttons>
```

#### Button With Loader

```jsx
const [loader, setLoader] = useState(false);
const clickBtn = () => {
    setLoader(true);
    setTimeout(() => {
        setLoader(false);
    }, 1000);
};
<Buttons
    loader={loader}
    onClick={() => {
        log("onClick Button", 1);
        clickBtn();
    }}
>
    Button
</Buttons>;
```

#### Button Icon

```jsx
import SVGEye from "@/svg/eye";
<Buttons
    icon={
        <span className="m-r-10">
            <SVGEye />
        </span>
    }
    iconPosition="left" // left or right
>
    Button
</Buttons>;
```

### Inputs

[doc](https://templatenextjs.vercel.app/doc/inputs)

Import Required

```jsx
import Input from "@/components/Input";
import InputPassword from "@/components/Input/Password";

import ERRORINPUTS from "@/data/error/inputs";
import VALIDATORINPUTS from "@/data/validations/inputs";
```

#### Email

```jsx
<Input
    label="Email"
    type="email"
    placeholder="Your Email"
    defaultValue="defaul@email.com"
    yup={VALIDATORINPUTS.email}
    onChange={(e) => {
        log("Change Email", e, "aqua");
    }}
    onBlur={(e) => {
        log("Blur Email", e, "aqua");
    }}
    onChangeValidate={async (value) => {
        log("onChangeValidate Email", value, "aqua");
        value = value.replaceAll("yopmail.com", "gmail.com");
        return value;
    }}
    onChangeValidateBeforeYup={async (value) => {
        log("onChangeValidateBeforeYup Email", value, "aqua");
        if (value.indexOf("yopmail2.com") != -1) {
            throw {
                message: ERRORINPUTS.email.invalid,
            };
        }
    }}
    onChangeValidateAfterYup={async (value) => {
        log("onChangeValidateAfterYup Email", value, "aqua");
        if (await exitsEmail(value)) {
            throw {
                message: ERRORINPUTS.email.invalid,
            };
        }
    }}
/>
```

#### Text

```jsx
<Input
    label="Name"
    type="text"
    placeholder="Your Name"
    defaultValue="defaul name"
    yup={VALIDATORINPUTS.name}
    onChange={(e) => {
        log("Change Name", e, "aqua");
    }}
    onBlur={(e) => {
        log("Blur Name", e, "aqua");
    }}
    onChangeValidate={async (value) => {
        log("onChangeValidate Name", value, "aqua");
        value = value.replaceAll(/[\d]/g, "");
        return value;
    }}
    onChangeValidateBeforeYup={async (value) => {
        log("onChangeValidateBeforeYup Name", value, "aqua");
        if (value.indexOf("admin") != -1) {
            throw {
                message: ERRORINPUTS.name.invalid,
            };
        }
    }}
    onChangeValidateAfterYup={async (value) => {
        log("onChangeValidateAfterYup Name", value, "aqua");
        if (await exitsName(value)) {
            throw {
                message: ERRORINPUTS.name.invalid,
            };
        }
    }}
/>
```

#### Password

```jsx
<InputPassword
    label="Password"
    placeholder="Your Password"
    defaultValue="defaul Password"
    yup={VALIDATORINPUTS.password}
    onChange={(e) => {
        log("Change Password", e, "aqua");
    }}
    onBlur={(e) => {
        log("Blur Password", e, "aqua");
    }}
    onChangeValidate={async (value) => {
        log("onChangeValidate Password", value, "aqua");
        return value;
    }}
    onChangeValidateBeforeYup={async (value) => {
        log("onChangeValidateBeforeYup Password", value, "aqua");
    }}
    onChangeValidateAfterYup={async (value) => {
        log("onChangeValidateAfterYup Password", value, "aqua");
    }}
/>
```

#### Url

```jsx
<Input
    label="Web Site"
    type="url"
    placeholder="Your Web Site"
    defaultValue="defaul_Web_Site.com"
    yup={VALIDATORINPUTS.url}
    onChange={(e) => {
        log("Change Web Site", e, "aqua");
    }}
    onBlur={(e) => {
        log("Blur Web Site", e, "aqua");
    }}
    onChangeValidate={async (value) => {
        log("onChangeValidate Web Site", value, "aqua");
        value = value.replaceAll(/[\d]/g, "");
        return value;
    }}
    onChangeValidateBeforeYup={async (value) => {
        log("onChangeValidateBeforeYup Web Site", value, "aqua");
        if (value.indexOf("admin.com") != -1) {
            throw {
                message: ERRORINPUTS.url.invalid,
            };
        }
    }}
    onChangeValidateAfterYup={async (value) => {
        log("onChangeValidateAfterYup Web Site", value, "aqua");
        if (await exitsUrl(value)) {
            throw {
                message: ERRORINPUTS.url.invalid,
            };
        }
    }}
/>
```

#### Number

```jsx
<Input
    label="Number"
    type="number"
    placeholder="Your Number"
    defaultValue="1"
    yup={VALIDATORINPUTS.number}
    onChange={(e) => {
        log("Change Number", e, "aqua");
    }}
    onBlur={(e) => {
        log("Blur Number", e, "aqua");
    }}
    onChangeValidate={async (value) => {
        log("onChangeValidate Number", value, "aqua");
        value = value.replaceAll(/[^\d]/g, "");
        value =
            value == "" ? value : Math.max(Math.min(parseFloat(value), 10), 0);
        return value;
    }}
/>
```

### Select

```jsx
import Select from "@/components/Select";

import VALIDATORINPUTS from "@/data/validations/inputs";

<Select
    label="Options"
    placeholder="Select"
    defaultValue={{
        text: "Option 2",
        value: "2",
    }}
    yup={VALIDATORINPUTS.options}
    options={[
        {
            text: "Option 1",
            value: "1",
            html: (
                <div className="flex flex-align-center">
                    <Image src="imageSmall.png" className="width-20 m-r-10" />
                    Option 1
                </div>
            ),
        },
        {
            text: "Option 2",
            value: "2",
        },
        {
            text: "Option 3",
            value: "3",
        },
        {
            text: "hola 3",
            value: "3",
        },
        {
            text: "ppppp 3",
            value: "3",
        },
        {
            text: "rrrr 3",
            value: "3",
        },
    ]}
    onChange={(e) => {
        log("Change Select", e, "aqua");
    }}
    onBlur={(e) => {
        log("Blur Select", e, "aqua");
    }}
/>;
```

## data

It is forder for save information static for pages

## functions

It is forder for create your funcions for proyect

## pages

It is forder for create your pages for proyect

## server

It is forder for create your getServerSideProps and getStaticProps for proyect

## Styles

It is forder for save your style and style static for proyect

### Responsive

| Ruel | Media               |
| ---- | ------------------- |
| sm   | (min-width: 575px)  |
| md   | (min-width: 768px)  |
| lg   | (min-width: 992px)  |
| xl   | (min-width: 1200px) |

### Font Example Use

| Property Css | Component Jsx                          | Value Css             | Style             |
| ------------ | -------------------------------------- | --------------------- | ----------------- |
| font-size    | `<h1 className="font-20">Text</h1>`    | (20px / 16px \* 1rem) | font-size:1.25rem |
| font-weight  | `<h1 className="font-w-700">Text</h1>` | 700                   | font-weight:700   |
| line-height  | `<h1 className="line-h-15">Text</h1>`  | (15/10)               | line-height:1.5   |

#### Font Responsive Example Use

| Property Css | Component Jsx                             |
| ------------ | ----------------------------------------- |
| font-size    | `<h1 className="font-sm-20">Text</h1>`    |
| font-weight  | `<h1 className="font-md-w-700">Text</h1>` |
| line-height  | `<h1 className="line-h-lg-15">Text</h1>`  |

### Heght Example Use

| Property Css | Component Jsx                            | Value Css             | Style          |
| ------------ | ---------------------------------------- | --------------------- | -------------- |
| height       | `<h1 className="height-20">Text</h1>`    | (20px / 16px \* 1rem) | height:1.25rem |
| height       | `<h1 className="height-vh-20">Text</h1>` | 20vh                  | height:20vh    |
| height       | `<h1 className="height-p-20">Text</h1>`  | 20%                   | height:20%     |

#### Heght Responsive Example Use

| Property Css | Component Jsx                               |
| ------------ | ------------------------------------------- |
| height       | `<h1 className="height-sm-20">Text</h1>`    |
| height       | `<h1 className="height-xl-vh-20">Text</h1>` |
| height       | `<h1 className="height-md-p-20">Text</h1>`  |

### Width Example Use

| Property Css | Component Jsx                           | Value Css             | Style         |
| ------------ | --------------------------------------- | --------------------- | ------------- |
| width        | `<h1 className="width-20">Text</h1>`    | (20px / 16px \* 1rem) | width:1.25rem |
| width        | `<h1 className="width-vh-20">Text</h1>` | 20vh                  | width:20vh    |
| width        | `<h1 className="width-p-20">Text</h1>`  | 20%                   | width:20%     |

#### Width Responsive Example Use

| Property Css | Component Jsx                              |
| ------------ | ------------------------------------------ |
| width        | `<h1 className="width-sm-20">Text</h1>`    |
| width        | `<h1 className="width-xl-vh-20">Text</h1>` |
| width        | `<h1 className="width-md-p-20">Text</h1>`  |

### Margin Example Use

| Property Css  | Component Jsx                        | Value Css             | Style                                                |
| ------------- | ------------------------------------ | --------------------- | ---------------------------------------------------- |
| margin        | `<h1 className="m-20">Text</h1>`     | (20px / 16px \* 1rem) | margin:1.25rem                                       |
| margin        | `<h1 className="m-auto">Text</h1>`   | auto                  | margin:auto                                          |
| margin-top    | `<h1 className="m-t-20">Text</h1>`   | (20px / 16px \* 1rem) | margin-top:1.25rem                                   |
| margin-top    | `<h1 className="m-t-auto">Text</h1>` | auto                  | margin-top:auto                                      |
| margin-left   | `<h1 className="m-l-20">Text</h1>`   | (20px / 16px \* 1rem) | margin-left:1.25rem                                  |
| margin-right  | `<h1 className="m-r-17">Text</h1>`   | (17px / 16px \* 1rem) | margin-right:1.0625rem                               |
| margin-bottom | `<h1 className="m-b-22">Text</h1>`   | (22px / 16px \* 1rem) | margin-bottom:1.375rem                               |
| margin x      | `<h1 className="m-h-25">Text</h1>`   | (25px / 16px \* 1rem) | margin-left:1.5625rem; <br/> margin-right:1.5625rem; |
| margin y      | `<h1 className="m-v-24">Text</h1>`   | (24px / 16px \* 1rem) | margin-top:1.5rem; <br/> margin-bottom:1.5rem;       |

#### Margin Responsive Example Use

| Property Css  | Component Jsx                           |
| ------------- | --------------------------------------- |
| margin        | `<h1 className="m-sm-20">Text</h1>`     |
| margin-top    | `<h1 className="m-lg-t-auto">Text</h1>` |
| margin-left   | `<h1 className="m-xl-l-20">Text</h1>`   |
| margin-right  | `<h1 className="m-md-r-17">Text</h1>`   |
| margin-bottom | `<h1 className="m-sm-b-22">Text</h1>`   |
| margin x      | `<h1 className="m-lg-h-25">Text</h1>`   |
| margin y      | `<h1 className="m-xl-v-24">Text</h1>`   |

### Padding Example Use

| Property Css   | Component Jsx                        | Value Css             | Style                                                  |
| -------------- | ------------------------------------ | --------------------- | ------------------------------------------------------ |
| padding        | `<h1 className="p-20">Text</h1>`     | (20px / 16px \* 1rem) | padding:1.25rem                                        |
| padding        | `<h1 className="p-auto">Text</h1>`   | auto                  | padding:auto                                           |
| padding-top    | `<h1 className="p-t-20">Text</h1>`   | (20px / 16px \* 1rem) | padding-top:1.25rem                                    |
| padding-top    | `<h1 className="p-t-auto">Text</h1>` | auto                  | padding-top:auto                                       |
| padding-left   | `<h1 className="p-l-20">Text</h1>`   | (20px / 16px \* 1rem) | padding-left:1.25rem                                   |
| padding-right  | `<h1 className="p-r-17">Text</h1>`   | (17px / 16px \* 1rem) | padding-right:1.0625rem                                |
| padding-bottom | `<h1 className="p-b-22">Text</h1>`   | (22px / 16px \* 1rem) | padding-bottom:1.375rem                                |
| padding x      | `<h1 className="p-h-25">Text</h1>`   | (25px / 16px \* 1rem) | padding-left:1.5625rem; <br/> padding-right:1.5625rem; |
| padding y      | `<h1 className="p-v-24">Text</h1>`   | (24px / 16px \* 1rem) | padding-top:1.5rem; <br/> padding-bottom:1.5rem;       |

#### Padding Responsive Example Use

| Property Css   | Component Jsx                           |
| -------------- | --------------------------------------- |
| padding        | `<h1 className="p-sp-20">Text</h1>`     |
| padding-top    | `<h1 className="p-lg-t-auto">Text</h1>` |
| padding-left   | `<h1 className="p-xl-l-20">Text</h1>`   |
| padding-right  | `<h1 className="p-md-r-17">Text</h1>`   |
| padding-bottom | `<h1 className="p-sm-b-22">Text</h1>`   |
| padding x      | `<h1 className="p-lg-h-25">Text</h1>`   |
| padding y      | `<h1 className="p-xl-v-24">Text</h1>`   |

### Position Example Use

| Property Css | Component Jsx                     | Value Css | Style             |
| ------------ | --------------------------------- | --------- | ----------------- |
| position     | `<h1 className="pos-r">Text</h1>` | relative  | position:relative |
| position     | `<h1 className="pos-a">Text</h1>` | absolute  | position:absolute |
| position     | `<h1 className="pos-f">Text</h1>` | fixed     | position:fixed    |
| position     | `<h1 className="pos-s">Text</h1>` | static    | position:static   |

#### Position Responsive Example Use

| Property Css | Component Jsx                        |
| ------------ | ------------------------------------ |
| position     | `<h1 className="pos-sm-r">Text</h1>` |
| position     | `<h1 className="pos-md-a">Text</h1>` |
| position     | `<h1 className="pos-lg-f">Text</h1>` |
| position     | `<h1 className="pos-xl-s">Text</h1>` |

### Text Example Use

| Property Css    | Component Jsx                                         | Value Css       | Style                        |
| --------------- | ----------------------------------------------------- | --------------- | ---------------------------- |
| text-align      | `<h1 className="text-left">Text</h1>`                 | left            | text-align:left              |
| text-align      | `<h1 className="text-center">Text</h1>`               | center          | text-align:center            |
| text-align      | `<h1 className="text-right">Text</h1>`                | right           | text-align:right             |
| text-decoration | `<h1 className="text-decoration-none">Text</h1>`      | text-decoration | text-decoration:none         |
| text-decoration | `<h1 className="text-decoration-through">Text</h1>`   | text-decoration | text-decoration:line-through |
| text-decoration | `<h1 className="text-decoration-underline">Text</h1>` | text-decoration | text-decoration:underline    |
| text-decoration | `<h1 className="text-decoration-overline">Text</h1>`  | text-decoration | text-decoration:overline     |

#### Text Responsive Example Use

| Property Css    | Component Jsx                                            |
| --------------- | -------------------------------------------------------- |
| text-align      | `<h1 className="text-md-left">Text</h1>`                 |
| text-align      | `<h1 className="text-sm-center">Text</h1>`               |
| text-align      | `<h1 className="text-lg-right">Text</h1>`                |
| text-decoration | `<h1 className="text-xl-decoration-none">Text</h1>`      |
| text-decoration | `<h1 className="text-md-decoration-through">Text</h1>`   |
| text-decoration | `<h1 className="text-sm-decoration-underline">Text</h1>` |
| text-decoration | `<h1 className="text-lg-decoration-overline">Text</h1>`  |

### Top Example Use

| Property Css | Component Jsx                        | Value Css             | Style       |
| ------------ | ------------------------------------ | --------------------- | ----------- |
| top          | `<h1 className="top-40">Text</h1>`   | (40px / 16px \* 1rem) | top: 2.5rem |
| top          | `<h1 className="top-p-40">Text</h1>` | 40%                   | top: 40%    |

#### Top Responsive Example Use

| Property Css | Component Jsx                           |
| ------------ | --------------------------------------- |
| top          | `<h1 className="top-md-40">Text</h1>`   |
| top          | `<h1 className="top-lg-p-40">Text</h1>` |

### Left Example Use

| Property Css | Component Jsx                         | Value Css             | Style        |
| ------------ | ------------------------------------- | --------------------- | ------------ |
| left         | `<h1 className="left-40">Text</h1>`   | (40px / 16px \* 1rem) | left: 2.5rem |
| left         | `<h1 className="left-p-40">Text</h1>` | 40%                   | left: 40%    |

#### Left Responsive Example Use

| Property Css | Component Jsx                            |
| ------------ | ---------------------------------------- |
| left         | `<h1 className="left-md-40">Text</h1>`   |
| left         | `<h1 className="left-lg-p-40">Text</h1>` |

### Right Example Use

| Property Css | Component Jsx                          | Value Css             | Style         |
| ------------ | -------------------------------------- | --------------------- | ------------- |
| right        | `<h1 className="right-40">Text</h1>`   | (40px / 16px \* 1rem) | right: 2.5rem |
| right        | `<h1 className="right-p-40">Text</h1>` | 40%                   | right: 40%    |

#### Right Responsive Example Use

| Property Css | Component Jsx                             |
| ------------ | ----------------------------------------- |
| right        | `<h1 className="right-md-40">Text</h1>`   |
| right        | `<h1 className="right-lg-p-40">Text</h1>` |

### Bottom Example Use

| Property Css | Component Jsx                           | Value Css             | Style          |
| ------------ | --------------------------------------- | --------------------- | -------------- |
| bottom       | `<h1 className="bottom-40">Text</h1>`   | (40px / 16px \* 1rem) | bottom: 2.5rem |
| bottom       | `<h1 className="bottom-p-40">Text</h1>` | 40%                   | bottom: 40%    |

#### Bottom Responsive Example Use

| Property Css | Component Jsx                              |
| ------------ | ------------------------------------------ |
| bottom       | `<h1 className="bottom-md-40">Text</h1>`   |
| bottom       | `<h1 className="bottom-lg-p-40">Text</h1>` |

### z-index Example Use

| Property Css | Component Jsx                          | Value Css | Style       |
| ------------ | -------------------------------------- | --------- | ----------- |
| z-index      | `<h1 className="z-index-5">Text</h1>`  | 5         | z-index: 5  |
| z-index      | `<h1 className="z-index--5">Text</h1>` | -5        | z-index: -5 |

#### z-index Responsive Example Use

| Property Css | Component Jsx                             |
| ------------ | ----------------------------------------- |
| z-index      | `<h1 className="z-index-md-5">Text</h1>`  |
| z-index      | `<h1 className="z-index-lg--5">Text</h1>` |

### Display Example Use

| Property Css | Component Jsx                              | Value Css                | Style                             |
| ------------ | ------------------------------------------ | ------------------------ | --------------------------------- |
| display      | `<h1 className="d-flex">Text</h1>`         | flex !important;         | display: flex !important;         |
| display      | `<h1 className="d-inline-block">Text</h1>` | inline-block !important; | display: inline-block !important; |
| display      | `<h1 className="d-block">Text</h1>`        | block !important;        | display: block !important;        |
| display      | `<h1 className="d-none">Text</h1>`         | none !important;         | display: none !important;         |

#### Display Responsive Example Use

| Property Css | Component Jsx                                 |
| ------------ | --------------------------------------------- |
| display      | `<h1 className="d-md-flex">Text</h1>`         |
| display      | `<h1 className="d-sm-inline-block">Text</h1>` |
| display      | `<h1 className="d-xl-block">Text</h1>`        |
| display      | `<h1 className="d-lg-none">Text</h1>`         |

### BoxShadow Example Use

| Property Css | Component Jsx                                                                                                                        | Value Css                                                  | Style                                                              |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------- | ------------------------------------------------------------------ |
| box-shadow   | `<h1 className="box-shadow box-shadow-inset box-shadow-s-1 box-shadow-black">Text</h1>`                                              | inset 0 0 0 (1/16)rem var(--black)                         | box-shadow: inset 0 0 0 0.0625rem var(--black)                     |
| box-shadow   | `<h1 className="box-shadow">Text</h1>`                                                                                               | box-shadow: normal                                         |                                                                    |
| box-shadow   | `<h1 className="box-shadow-inset">Text</h1>`                                                                                         | box-shadow: inset                                          |                                                                    |
| box-shadow   | `<h1 className="box-shadow-x-5">Text</h1>`                                                                                           | box-shadow: x = (5/16)rem                                  |                                                                    |
| box-shadow   | `<h1 className="box-shadow-y-9">Text</h1>`                                                                                           | box-shadow: y = (9/16)rem                                  |                                                                    |
| box-shadow   | `<h1 className="box-shadow-b-7">Text</h1>`                                                                                           | box-shadow: blur = (7/16)rem                               |                                                                    |
| box-shadow   | `<h1 className="box-shadow-s-3">Text</h1>`                                                                                           | box-shadow: size = (3/16)rem                               |                                                                    |
| box-shadow   | `<h1 className="box-shadow-c-blue">Text</h1>`                                                                                        | box-shadow: color = var(--blue)                            |                                                                    |
| box-shadow   | `<h1 className="box-shadow box-shadow-inset box-shadow-x-9 box-shadow-y-8 box-shadow-b-7 box-shadow-s-6 box-shadow-black">Text</h1>` | inset (9/16)rem (8/16)rem (7/16)rem (6/16)rem var(--black) | box-shadow: inset 0.5625rem 0.5rem 0.4375rem 0.375rem var(--black) |

### Flex Example Use

| Property Css    | Component Jsx                                    | Value Css                               | Style                              |
| --------------- | ------------------------------------------------ | --------------------------------------- | ---------------------------------- |
| flex            | `<h1 className="flex">Text</h1>`                 | flex; <br/> wrap;                       | display: flex;<br/> flex-wrap:wrap |
| col-[1-12]      | `<h1 className="flex-x">Text</h1>`               | calc(#{100% \* x / 12} - var(--gap, 0)) | flex-basis:calc(size - gap)        |
| gap             | `<h1 className="flex-gap-10">Text</h1>`          | (10 /16)rem                             | --gap: 0.625                       |
| flex-direction  | `<h1 className="flex-row">Text</h1>`             | row                                     | flex-direction: row;               |
| flex-direction  | `<h1 className="flex-row-reverse">Text</h1>`     | row-reverse                             | flex-direction: row-reverse;       |
| flex-direction  | `<h1 className="flex-column">Text</h1>`          | column                                  | flex-direction: column;            |
| flex-direction  | `<h1 className="flex-column-reverse">Text</h1>`  | column-reverse                          | flex-direction: column-reverse;    |
| flex-wrap       | `<h1 className="flex-nowrap">Text</h1>`          | nowrap                                  | flex-wrap: nowrap;                 |
| align-items     | `<h1 className="flex-align-stretch">Text</h1>`   | stretch                                 | align-items: stretch;              |
| align-items     | `<h1 className="flex-align-start">Text</h1>`     | start                                   | align-items: flex-start;           |
| align-items     | `<h1 className="flex-align-center">Text</h1>`    | center                                  | align-items: center;               |
| align-items     | `<h1 className="flex-align-end">Text</h1>`       | end                                     | align-items: flex-end;             |
| justify-content | `<h1 className="flex-justify-around">Text</h1>`  | space-around                            | justify-content: space-around;     |
| justify-content | `<h1 className="flex-justify-between">Text</h1>` | space-between                           | justify-content: space-between;    |
| justify-content | `<h1 className="flex-justify-left">Text</h1>`    | flex-start                              | justify-content: flex-start;       |
| justify-content | `<h1 className="flex-justify-center">Text</h1>`  | center                                  | justify-content: center;           |
| justify-content | `<h1 className="flex-justify-right">Text</h1>`   | flex-end                                | justify-content: flex-end;         |

#### Flex Responsive Example Use

| Property Css    | Component Jsx                                       |
| --------------- | --------------------------------------------------- |
| col-[1-12]      | `<h1 className="flex-md-3">Text</h1>`               |
| gap             | `<h1 className="flex-lg-gap-10">Text</h1>`          |
| flex-direction  | `<h1 className="flex-sm-row">Text</h1>`             |
| flex-direction  | `<h1 className="flex-xl-row-reverse">Text</h1>`     |
| flex-direction  | `<h1 className="flex-md-column">Text</h1>`          |
| flex-direction  | `<h1 className="flex-sm-column-reverse">Text</h1>`  |
| flex-wrap       | `<h1 className="flex-lg-nowrap">Text</h1>`          |
| align-items     | `<h1 className="flex-sm-align-stretch">Text</h1>`   |
| align-items     | `<h1 className="flex-md-align-start">Text</h1>`     |
| align-items     | `<h1 className="flex-lg-align-center">Text</h1>`    |
| align-items     | `<h1 className="flex-xl-align-end">Text</h1>`       |
| justify-content | `<h1 className="flex-sm-justify-around">Text</h1>`  |
| justify-content | `<h1 className="flex-md-justify-between">Text</h1>` |
| justify-content | `<h1 className="flex-lg-justify-left">Text</h1>`    |
| justify-content | `<h1 className="flex-xm-justify-center">Text</h1>`  |
| justify-content | `<h1 className="flex-md-justify-right">Text</h1>`   |

## svg

It is forder for save your svg for proyect

## Developer

[Francisco Blanco](https://franciscoblanco.vercel.app/)
